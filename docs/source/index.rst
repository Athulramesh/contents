.. contents documentation master file, created by
   sphinx-quickstart on Thu Sep 13 22:05:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Decentralized Application Development 101!
==========================================

Step 1: Installing & Running the Ethereum TestRPC
-------------------------------------------------

The Ethereum TestRPC is a Node.js Ethereum client for the testing and developing smart
contracts. Because it's based on Node.js, we need Node.js installed along with NPM (Node
Package Manager) to install it.

Check:

> node -v

> npm -v

If either of these commands go unrecognized, visit Nodejs.org and download the appropriate
installer. Run it through all of the default options.

Use NPM to install the Ethereumjs-testrpc:

> npm install -g ethereumjs-testrpc

Once finished, run the following command to start it:

> testrpc

This provides you with 10 different accounts and private keys, along with a local server at
localhost:8545.

Step 2: Installing Web3.js
----------------------------

Web3.js is the official Ethereum Javascript API. You use it to interact with your Ethereum
smart contracts.

Before we can install it, let's create a project folder in a new console window:

> mkdir coursetro-eth

> cd coursetro-eth

Next, run the npm init command to create a package.json file, which will store project
dependencies:

> npm init

Hit enter through all of the prompts. Next, run the following command to install web3.js:

> npm install ethereum/web3.js --save


Step 3: Changing the Environment in Remix
------------------------------------------

Switch over to the Remix IDE, click on the Run tab, and then change the Environment
dropdown from JavaScript VM to Web3 Provider.

Hit "OK" and then specify the testrpc localhost address (by default, it's
http://localhost:8545).

This means that instead of deploying and testing in the Javascript VM, we're now using the
TestRPC client on your computer.

If you haven't been following along since the previous lesson, paste in this contract in a new solidity file called "Test.sol":

 pragma solidity ^0.4.18;

 contract Test {

        string fName;
        uint age;

	function setInstructor(string _fName, uint _age) public {
		fName = _fName;
		age = _age;

	   	}

	function getInstructor() public constant returns (string, uint) {
		return (fName, age);

		}
	}


Hit Create. We will need the address of this contract shortly, so leave this window open.


Step 4: Creating the UI
------------------------

Open up your preferred code editor (I use Brackets) with the project folder we created. Here, you'll notice a node_modules folder, which includes web3 that we installed via npm earlier. Let's create an index.html in the project folder. We're not going to create anything too fancy in terms of a UI, but we'll have some limited CSS, and a UI that consists of a place that retrieves the Instructor's name and age from the
getInstructor() function, and a form with 2 input fields for a name and age, which will be set via jQuery from 2 input textfields.

	<!DOCTYPE html>

	<html lang="en">

	<head>

		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta http-equiv="X-UA-Compatible" content="ie=edge">

		<title>Document</title>


		<link rel="stylesheet" type="text/css" href="main.css">

		<script src="./node_modules/web3/dist/web3.min.js"></script>


	</head>

	<body>

		<div class="container">


		<h1>Coursetro Instructor</h1>


		<h2 id="instructor"></h2>


		<label for="name" class="col-lg-2 control-label">Instructor
	Name</label>

		<input id="name" type="text">


		<label for="name" class="col-lg-2 control-label">Instructor
	Age</label>

		<input id="age" type="text">


		<button id="button">Update Instructor</button>

	</div>


	<script src="https://code.jquery.com/jquery-

	3.2.1.slim.min.js"></script>


	<script>

	// Our future code here..

	</script>


     </body>

     </html>

	
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
